
### Описание функционала

    1. Начало работы - окно регистрации, логин/почта + пароль, предполагается использовать единую
	систему аутотентификации политеха.  
    2. Главная (домашняя страница) - будут разделы  
        1. Моё расписание
        2. Мои группы (список групп по номерам, получается из запроса на расписание преподавателя, хранится в бд)
        3. Наличие кнопки "Выход" 
    3. В разделе расписание отображается список занятий конкретно для этого 
	пользователя (число, день недели, время, аудитория) на этой неделе.
	Предполагается наличие кнопок "предыдущая неделя" и "следующая неделя".
    4. В разделе "мои группы" есть "ссылочные кнопки" групп, наподобие того, 
	что есть сейчас в обычном политеховском расписании.
	На них можно нажать и перейти на следующую страницу, которая содержит таблицу со списком группы и датами занятий,
	в таблице можно проставлять оценки, а так же отметку об отсуствии/присутствии. 

### Пока под обсуждением

    1. Обсуждается возможность наличия кнопки "сохранить изменения" или же сделать автосохранение на странице с таблицей.
    2. Обсуждается добавление функционала для просмотра отчетности в конце семестра

### User Stories

    1. Я хочу использовать приложение, для этого я захожу на его страницу входа в браузере и регистрируюсь, 
	для регистрации я нажимаю кнопку "зарегистрироваться" и попадаю на страницу регистрации, 
	где ввожу свою рабочую почту (рабочую == полученную от политеха) и остальные личные данные. 
	После регистрации я попадаю на страницу с объявлением о подтверждении регистрации. 
	Для подтверждения я захожу на рабочую почту и перехожу по ссылке, которую мне прислали, 
	попадая снова на главную страницу.
    2. Если я уже зарегистрирован, я ввожу login/e-mail и пароль на странице входа и попадаю на 
	главную страницу с меню приложения, теперь приложение можно использовать.
    3. Я хочу посмотреть расписание занятий, для этого я логинюсь в приложение, и выбираю в меню 
	вкладку "расписание", на котором вижу требуемую информацию о занятии (даты, время, место).
    4. Я хочу посмотреть расписание на другое неделе (не на текущей), для этого я захожу на вкладку 
	расписание и нажимаю требуемую кнопку "предыдущая/следующая неделя".
    5. На занятии я хочу отмечать посещаемость студентов, либо выставлять им оценки за занятие, 
	я открываю приложение, захожу в раздел "мои группы", выбираю нужную группу
	и в открывашейся таблице выставляю желаемые значения.
    6. Я хочу выйти из приложения. Я нажимаю кнопку выход на странице приложения, на которой я сейчас нахожусь.

