### How to deploy this app on a local machine

1. Install the MongoDB  
	[Manual - install MondoDB Community Server](https://metanit.com/nosql/mongodb/1.2.php)  

2. Install tomcat server  
	Download Tomcat Server from the [official site](https://tomcat.apache.org/download-90.cgi)  
	Unpack archive to Program Files folder (or wherever you want)  
	
3. Install IDEA ultimate version  
	[How to get student license](https://www.jetbrains.com/student/)  
	Or you can use trial 30-day version  
	You should have last full version of repo (update your local repository)  
	Open IDEA   
	Create new project from existing sources  
	![New project](../pictures/idea_proj_create.png)  
	Select project/pom.xml file  
	Finish the installation  
	  
4. Deployment  
  
	Start mongod.exe  
	Start mongo.exe  
	Create db with name "tchdb" (write in mongo console "use tchdb")  
	Set run configuration for app  
	![edit conf](../pictures/idea_edit_conf.png)  
	Set tomcat server config  
	![edit conf tomcat](../pictures/idea_edit_conf_tm.png)  
	Set deployment artifact (war exploded)  
	![edit conf artifact](../pictures/idea_edit_conf_art.png)  