## Teacher schedule release notes

Teacher schedule - web application for teachers

### Version 0.9, 21.04.2019

New + improvements

1. Start page for entrance, one button works: go to menu page
2. Menu page for one user, two options available: go to schedule page, go to groups page
3. Schedule page
4. Groups page: list of groups with opportunity to go to personal group page
5. Personal group page: List of students, opportunity to change attendance, rate student, safe changes, or watch previous and featuring dates


### Version 1.0, 29.04.2019
New
   1. New design variant
   2. Working schedule page
   3. Mechanism of adding works (not finished)

Fixed
   1. Bugs with selects
   2. Bugs on attendance page

### Version 1.1, 13.05.2019

Improvements
   1. Fixed bug on index page
   2. Added script of mechanism of edit works
   3. Added class name in schedule page
   4. Changed date format in  schedule page

### Version 1.2, 20.05.2019
New
 1. Added opportunity to delete works
 2. Added scroll works

Fixed
 1. Size of string in table of students
 2. Changed id styles for classes
 3. Positioning of drop-down menu
