# Teacher schedule

Application for teachers with a schedule, a list of exams and other useful things

Slack Workspace: [teacher schedule development workspace](https://sdp-lab-2019-4.slack.com) 

### Development team
 1. Andrey Trinogin
 2. Cherpakov Mikhail
 3. Ekaterina Kasyanova
 4. Gleb Savitski
 5. Olesya Galimova
 6. Valeria Lobzhanidze
 7. Yegor Oboyenkov
 
### [Team acces time](https://docs.google.com/spreadsheets/d/1OV9fQ6Spu3aUNXGC_EO-4gGZ6Jh3dhdV59db8dxiWYE/edit#gid=0)
 
---
### Code style and commits rules

##### Comments
 1. In English
 2. Multi-line (always with closing sequence)
 3. From codestyle standarts requirements

##### Code style
 1. Java: [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)
 2. JavaScript: [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html#naming-rules-common-to-all-identifiers)
 3. HTML/CSS: [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide)
 4. SQL: [SQL Style Guide](https://www.sqlstyle.guide/ru/)

##### Commits
 When writing commits, we follow the standard [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.3/)
 
 In the message to the commit you should put a link to the task that was performed in the format (in a separate line):
 >issue #(number of issue)

 Additionally: [Explanations in Russian](https://habr.com/ru/company/yandex/blog/431432/)

##### Merge branches
 1) Merging any branches to branch 'develop' is available only by merge-request to @Andrey.T or  @lera.lobzhanidze
 2) Branch develop can be merged to branch master only by general decision of command

##### Model of branches
 We use this model [a-successful-git-branching-model](https://nvie.com/posts/a-successful-git-branching-model/)  
 In russian [a-successful-git-branching-model-in-russian](https://m.habr.com/ru/post/106912/)
##### Labels
 1. Usual tasks
    1. When you start a task, uncheck the "todo" label and tick the "in progress" label
    2. Asignee it to yourself
    3. At the end of the task, put a mark "done" (removing the mark "in progress")

 2. Label "Research" you can not take on this task. These are common tasks, if you have done this, send a message about it to the “discussion of the issue”
 3. Label “General” - you can suggest to discuss your own task separation. You can not take on the task
 